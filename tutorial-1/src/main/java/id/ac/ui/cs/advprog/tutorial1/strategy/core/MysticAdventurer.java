package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    public MysticAdventurer() {
        super();
        this.setAttackBehavior(new AttackWithMagic());
        this.setDefenseBehavior(new DefendWithShield());
    }

    @Override
    public String getAlias() {
        return "Mystic";
    }
    //ToDo: Complete me
}
