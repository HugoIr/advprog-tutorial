package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
    }

    @Override
    public void update() {
        // this.getQuests().add(guild.getQuest());
        String quest = this.guild.getQuestType();
        if(quest.equals("D") || quest.equals("E")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
